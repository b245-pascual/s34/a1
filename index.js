
    // s34 activity Create several routes using REST API and Express JS methods to perform different tasks.

        const express = require('express'); 

        // localhost port number
        const port = 4000; 

        // store express module to app variable
        const app = express();

        // //streaming of data and automatically parse the incoming json from our request.
        app.use(express.json());


        //Mock data
        let items = [
            {
                name: "Shitzu",
                price: 8000,
                isActive: true
            },
            {
                name: "French Bulldog",
                price: 20000,
                isActive: true
            }

        ];

        // Homepage
        app.get("/home", (request, response) => response.send('Ready for rehoming'));

        // access mock-up items
        app.get("/items", (resquest, response) => response.send(items));

        // Delete
        app.delete("/delete-item", (request, response) => {
            const deletedItem = items.pop();
            response.send(deletedItem)
        })

        app.listen(port, ()=> console.log(`Server is running at port ${port}`))
